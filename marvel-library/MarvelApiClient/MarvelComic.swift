//
//  MarvelComic.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MarvelComic: MarvelApiObject {
    var comicIssueNumber: Int?
    
    convenience init(attributes: Dictionary<String, AnyObject>) {
        self.init()
        
        self.marvelId = attributes["id"] as? Int
        self.marvelName = attributes["title"] as? String
        self.marvelDescription = attributes["description"] as? String
        self.comicIssueNumber = attributes["issueNumber"] as? Int

        if let thumbnail = attributes["thumbnail"] as? Dictionary<String, AnyObject> {
            self.marvelImagePath = thumbnail["path"] as? String
            self.marvelImageExtension = thumbnail["extension"] as? String
        }
    }
}
