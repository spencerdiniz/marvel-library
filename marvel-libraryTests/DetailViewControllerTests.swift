//
//  DetailViewControllerTests.swift
//  marvel-library
//
//  Created by Spencer Diniz on 14/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import XCTest
@testable import marvel_library

class DetailViewControllerTests: XCTestCase {
    
    var detailViewController: DetailViewController?
    
    override func setUp() {
        super.setUp()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.detailViewController = storyboard.instantiateViewControllerWithIdentifier("DetailViewController") as? DetailViewController
        XCTAssertNotNil(self.detailViewController)
        
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelCharacter") as? Dictionary<String, AnyObject> {
            let marvelCharacter = MarvelCharacter(attributes: dictionary)
            self.detailViewController?.detailItem = marvelCharacter
        }
        else {
            XCTFail("Unable to load MarvelCharacter.json file.")
        }
    }
    
    func testTableView() {
        XCTAssertEqual(self.detailViewController?.numberOfSectionsInTableView(UITableView()), 2)
        XCTAssertEqual(self.detailViewController?.tableView(UITableView(), numberOfRowsInSection: 0), 6)
        XCTAssertEqual(self.detailViewController?.tableView(UITableView(), numberOfRowsInSection: 1), 3)
        
        if let nameCell = self.detailViewController?.tableView(UITableView(), cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 0)) as? DetailViewControllerTextTableViewCell {
            XCTAssertEqual(nameCell.titleTextLabel.text, "NAME")
            XCTAssertEqual(nameCell.descriptionTextLabel.text, "3-D Man")
        }
        else {
            XCTFail("Unable to create name cell.")
        }

        if let descriptionCell = self.detailViewController?.tableView(UITableView(), cellForRowAtIndexPath: NSIndexPath(forRow: 1, inSection: 0)) as? DetailViewControllerTextTableViewCell {
            XCTAssertEqual(descriptionCell.titleTextLabel.text, "DESCRIPTION")
            XCTAssertEqual(descriptionCell.descriptionTextLabel.text, "This is 3-D Man. He needs no description.")
        }
        else {
            XCTFail("Unable to create description cell.")
        }
        
        if let linkCell = self.detailViewController?.tableView(UITableView(), cellForRowAtIndexPath: NSIndexPath(forRow: 0, inSection: 1)) {
            XCTAssertEqual(linkCell.textLabel?.text, "detail")
            XCTAssertEqual(linkCell.accessoryType, UITableViewCellAccessoryType.DisclosureIndicator)
        }
        else {
            XCTFail("Unable link cell.")
        }

        if let linkCell = self.detailViewController?.tableView(UITableView(), cellForRowAtIndexPath: NSIndexPath(forRow: 1, inSection: 1)) {
            XCTAssertEqual(linkCell.textLabel?.text, "wiki")
            XCTAssertEqual(linkCell.accessoryType, UITableViewCellAccessoryType.DisclosureIndicator)
        }
        else {
            XCTFail("Unable link cell.")
        }

        if let linkCell = self.detailViewController?.tableView(UITableView(), cellForRowAtIndexPath: NSIndexPath(forRow: 2, inSection: 1)) {
            XCTAssertEqual(linkCell.textLabel?.text, "comiclink")
            XCTAssertEqual(linkCell.accessoryType, UITableViewCellAccessoryType.DisclosureIndicator)
        }
        else {
            XCTFail("Unable link cell.")
        }
    }
}
