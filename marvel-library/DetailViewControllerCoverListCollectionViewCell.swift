//
//  DetailViewControllerCoverListCollectionViewCell.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class DetailViewControllerCoverListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImageImaveView: UIImageView!
    @IBOutlet weak var coverTitleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.resetCell()
    }
    
    override func prepareForReuse() {
        self.resetCell()
    }

    private func resetCell() {
        self.coverTitleLabel.text = nil
        self.coverImageImaveView.image = nil
        self.activityIndicator.stopAnimating()
    }
    
    func loadMarvelApiObject(marvelApiObject: MarvelApiObject) {
        self.activityIndicator.startAnimating()
        self.coverTitleLabel.text = marvelApiObject.marvelName
        MarvelApiClient.getMarvelImageForMarvelApiObject(marvelApiObject, marvelImageType: .PortraitThumbnail) { (image) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.coverImageImaveView.image = image
                self.activityIndicator.stopAnimating()
            })
        }
    }
}
