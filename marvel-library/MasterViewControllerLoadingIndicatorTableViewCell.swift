//
//  MasterViewControllerLoadingIndicatorTableViewCell.swift
//  marvel-library
//
//  Created by Spencer Diniz on 13/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MasterViewControllerLoadingIndicatorTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func prepareForReuse() {
        self.activityIndicator.startAnimating()
    }
}
