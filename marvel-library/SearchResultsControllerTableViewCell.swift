//
//  SearchResultsControllerTableViewCell.swift
//  marvel-library
//
//  Created by Spencer Diniz on 13/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class SearchResultsControllerTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageImageView: UIImageView!
    @IBOutlet weak var primaryTextLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = UIEdgeInsetsZero
    }
    
    override func prepareForReuse() {
        self.primaryTextLabel.text = nil
        self.thumbnailImageImageView.image = nil
        self.activityIndicator.stopAnimating()
    }
    
    func loadMarvelApiObject(marvelApiObject: MarvelApiObject) {
        self.activityIndicator.startAnimating()
        self.primaryTextLabel.text = marvelApiObject.marvelName
        MarvelApiClient.getMarvelImageForMarvelApiObject(marvelApiObject, marvelImageType: .SquareThumbnail) { (image) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.activityIndicator.stopAnimating()
                self.thumbnailImageImageView.image = image
            })
        }
    }
}
