# IMPORTANT README #

Download master branch (or tag "1.0.0") and build/run using **Xcode 7**. This project targets **iOS 9.2**.

![cocoapodslogo.gif](https://bitbucket.org/repo/4d7GXG/images/3196657819-cocoapodslogo.gif)

**OBS:** Since this project uses CocoaPods, you must open the workspace file (*marvel-library.xcworkspace*) to build/run. The project (*marvel-library.xcodeproj*) file will not work.

### Please consider: ###

* Used CocoaPods to integrate some third party libraries, most importantly Alamofire.
* Includes some unit tests for api model classes.
* Includes some unit tests for DetailViewController.
* Includes simple UI test for search flow.
* Additional unit and UI testing would require stubbing HTTP calls. I didn't think this would be necessary for the purpose of this challenge.

### Contact Info: ###
* **Name**: Spencer M. Diniz
* **E-mail**: [spencerdiniz@gmail.com](mailto:spencerdiniz@gmail.com)