//
//  MarvelEvent.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MarvelEvent: MarvelApiObject {
    var eventStart: NSDate?
    var eventEnd: NSDate?
    
    convenience init(attributes: Dictionary<String, AnyObject>) {
        self.init()
        
        self.marvelId = attributes["id"] as? Int
        self.marvelName = attributes["title"] as? String
        self.marvelDescription = attributes["description"] as? String

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let dateString = attributes["start"] as? String {
            self.eventStart =  dateFormatter.dateFromString(dateString)
        }

        if let dateString = attributes["end"] as? String {
            self.eventEnd =  dateFormatter.dateFromString(dateString)
        }

        if let thumbnail = attributes["thumbnail"] as? Dictionary<String, AnyObject> {
            self.marvelImagePath = thumbnail["path"] as? String
            self.marvelImageExtension = thumbnail["extension"] as? String
        }
    }
}
