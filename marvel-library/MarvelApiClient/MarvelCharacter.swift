//
//  MarvelCharacter.swift
//  marvel-library
//
//  Created by Spencer Diniz on 11/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MarvelCharacter: MarvelApiObject {
    var characterLastUpdate: NSDate?
    var availableComics: Int? = 0
    var availableSeries: Int? = 0
    var availableStories: Int? = 0
    var availableEvents: Int? = 0
    var relatedLinks: Array<MarvelLink> = []

    convenience init(attributes: Dictionary<String, AnyObject>) {
        self.init()

        self.marvelId = attributes["id"] as? Int
        self.marvelName = attributes["name"] as? String
        self.marvelDescription = attributes["description"] as? String
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        if let dateString = attributes["modified"] as? String {
            self.characterLastUpdate =  dateFormatter.dateFromString(dateString)
        }
        
        if let comics = attributes["comics"] as? Dictionary<String, AnyObject> {
            self.availableComics = comics["available"] as? Int
        }

        if let series = attributes["series"] as? Dictionary<String, AnyObject> {
            self.availableSeries = series["available"] as? Int
        }

        if let stories = attributes["stories"] as? Dictionary<String, AnyObject> {
            self.availableStories = stories["available"] as? Int
        }

        if let events = attributes["events"] as? Dictionary<String, AnyObject> {
            self.availableEvents = events["available"] as? Int
        }

        if let thumbnail = attributes["thumbnail"] as? Dictionary<String, AnyObject> {
            self.marvelImagePath = thumbnail["path"] as? String
            self.marvelImageExtension = thumbnail["extension"] as? String
        }
        
        if let urls = attributes["urls"] as? Array<Dictionary<String, AnyObject>> {
            for linkDictionary in urls {
                let marvelLink = MarvelLink(attributes: linkDictionary)
                self.relatedLinks.append(marvelLink)
            }
        }
    }
}
