//
//  FullScreenCoverListItemView.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class FullScreenCoverListItemView: UIView {
    
    @IBOutlet weak var coverItemImageImageView: UIImageView!
    @IBOutlet weak var coverItemPrimaryLabel: UILabel!
    @IBOutlet weak var coverItemSecondaryLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    func loadMarvelApiObject(marvelApiObject: MarvelApiObject) {
        self.activityIndicator.startAnimating()
        self.coverItemPrimaryLabel.text = marvelApiObject.marvelName;
        
        MarvelApiClient.getMarvelImageForMarvelApiObject(marvelApiObject, marvelImageType: .FullSize) { (image) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.coverItemImageImageView.image = image
                self.activityIndicator.stopAnimating()
            })
        }
    }
}
