//
//  DetailViewControllerCoverListTableViewCell.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class DetailViewControllerCoverListTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = UIEdgeInsetsZero
    }

    var datasource: Array<MarvelApiObject>! = [] {
        didSet {
            if self.collectionView != nil {
                self.collectionView.reloadData()
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    var actionForItemSelect: ((indexPath: NSIndexPath) -> Void)?
    var marvelCharacter: MarvelCharacter?
    
    override func prepareForReuse() {
        if self.collectionView != nil {
            self.datasource.removeAll()
            self.activityIndicator.startAnimating()
            self.collectionView.reloadData()
        }
    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.datasource.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DetailViewControllerCoverListCollectionViewCell", forIndexPath: indexPath) as! DetailViewControllerCoverListCollectionViewCell
        
        let marvelApiObject = self.datasource[indexPath.item]
        cell.loadMarvelApiObject(marvelApiObject)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.actionForItemSelect != nil {
            self.actionForItemSelect!(indexPath: indexPath)
        }
    }
}
