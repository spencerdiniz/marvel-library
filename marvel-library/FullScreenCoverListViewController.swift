//
//  FullScreenCoverListViewController.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit
import iCarousel

class FullScreenCoverListViewController: UIViewController, iCarouselDataSource, iCarouselDelegate {

    @IBOutlet weak var carousel: iCarousel!
    
    var datasource: Array<MarvelApiObject> = [] {
        didSet {
            if let _ = self.carousel {
                self.carousel.reloadData()
            }
        }
    }
    
    var currentListItemIndex: Int = 0 {
        didSet {
            if let _ = self.carousel {
                self.carousel.currentItemIndex = self.currentListItemIndex
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.carousel.type = .Linear
        self.carousel.pagingEnabled = true
        self.carousel.bounceDistance = 0.1
        
        self.carousel.currentItemIndex = self.currentListItemIndex
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
    }
    
    @IBAction func doneButtonAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return self.datasource.count
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.Spacing {
            return 1.0 + 10.0 / 320.0;
        }
        else {
            return value;
        }
    }

    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        
        let marvelApiObject = self.datasource[index]
        
        let coverItemView = NSBundle.mainBundle().loadNibNamed("FullScreenCoverListItemView", owner: self, options: nil).first as! FullScreenCoverListItemView
        
        coverItemView.loadMarvelApiObject(marvelApiObject)
        coverItemView.coverItemSecondaryLabel.text = "\(index + 1) / \(self.datasource.count)"
        
        return coverItemView
    }

}
