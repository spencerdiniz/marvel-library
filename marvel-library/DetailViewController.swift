//
//  DetailViewController.swift
//  marvel-library
//
//  Created by Spencer Diniz on 10/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {

    @IBOutlet weak var characterImageImageView: UIImageView!
    
    var backgroundImageView: UIImageView?
    var comicsCoverListCell: DetailViewControllerCoverListTableViewCell?
    var seriesCoverListCell: DetailViewControllerCoverListTableViewCell?
    var storiesCoverListCell: DetailViewControllerCoverListTableViewCell?
    var eventsCoverListCell: DetailViewControllerCoverListTableViewCell?
    
    var detailItem: AnyObject? {
        didSet {
            self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
        self.setupBlurredBackground()
        self.configureView()
    }
    
    func setupTableView() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 250.0
        
        var newHeaderFrame = self.tableView.tableHeaderView!.frame
        newHeaderFrame.size.width = UIScreen.mainScreen().bounds.width
        newHeaderFrame.size.height = UIScreen.mainScreen().bounds.width
        
        self.tableView.tableHeaderView!.frame = newHeaderFrame
        self.tableView.tableHeaderView!.backgroundColor = UIColor.clearColor()
    }
    
    func setupBlurredBackground() {
        let backgroundView = UIView(frame: UIScreen.mainScreen().bounds)
        self.backgroundImageView = UIImageView(frame: backgroundView.bounds)
        backgroundView.addSubview(self.backgroundImageView!)
        let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
        effectView.frame = backgroundView.bounds
        effectView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.3)
        backgroundView.addSubview(effectView)
        
        self.navigationController?.view.insertSubview(backgroundView, atIndex: 0)
    }
    
    func configureView() {
        if let marvelCharacter = self.detailItem as? MarvelCharacter {
            MarvelApiClient.getMarvelImageForMarvelApiObject(marvelCharacter, marvelImageType: .SquareThumbnail, completion: { (image) -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.characterImageImageView.image = image
                    self.backgroundImageView!.image = image
                })
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            let navController = (controllers[controllers.count-1] as! UINavigationController)
            navController.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
            navController.navigationBar.shadowImage = UIImage()
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        switch section {
        case 0:
            numberOfRows = 6
        case 1:
            if let marvelCharacter = self.detailItem as? MarvelCharacter {
                numberOfRows = marvelCharacter.relatedLinks.count
            }
        default:
            numberOfRows = 0
        }
        
        return  numberOfRows
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var headerHeight: CGFloat = 0.0
        
        if section == 1 {
            headerHeight = 21.0
        }
        
        return headerHeight
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var rowHeight = UITableViewAutomaticDimension

        if indexPath.section == 0 {
            if let marvelCharacter = self.detailItem as? MarvelCharacter {
                switch indexPath.row {
                case 2:
                    rowHeight = marvelCharacter.availableComics > 0 ? 250.0 : 37.0
                case 3:
                    rowHeight = marvelCharacter.availableSeries > 0 ? 250.0 : 37.0
                case 4:
                    rowHeight = marvelCharacter.availableStories > 0 ? 250.0 : 37.0
                case 5:
                    rowHeight = marvelCharacter.availableEvents > 0 ? 250.0 : 37.0
                default:
                    rowHeight = UITableViewAutomaticDimension
                }
            }
            else {
                rowHeight = 0.0
            }
        }
        if indexPath.section == 1 {
            rowHeight = 44.0
        }
        
        return rowHeight
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headerLabel = UILabel(frame: CGRect(x: 8.0, y: 0.0, width: 312.0, height: 21.0))
            headerLabel.font = UIFont.boldSystemFontOfSize(12.0)
            headerLabel.textColor = UIColor.redColor()
            headerLabel.text = "RELATED LINKS"

            let headerContainer = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.mainScreen().bounds.size.width, height: 21.0))
            headerContainer.addSubview(headerLabel)
            
            return headerContainer
        }
        
        return nil
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let marvelCharacter = self.detailItem as? MarvelCharacter {
                switch indexPath.row {
                case 0:
                    let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerTextTableViewCell") as! DetailViewControllerTextTableViewCell
                    cell.titleTextLabel.text = "NAME"
                    cell.descriptionTextLabel.text = marvelCharacter.marvelName
                    return cell
                case 1:
                    let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerTextTableViewCell") as! DetailViewControllerTextTableViewCell
                    cell.titleTextLabel.text = "DESCRIPTION"
                    cell.descriptionTextLabel.text = (marvelCharacter.marvelDescription ?? "").isEmpty ? "-" : marvelCharacter.marvelDescription
                    return cell
                case 2:
                    if self.comicsCoverListCell == nil {
                        self.comicsCoverListCell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerCoverListTableViewCell") as? DetailViewControllerCoverListTableViewCell
                        self.comicsCoverListCell!.activityIndicator.startAnimating()
                        self.comicsCoverListCell!.titleTextLabel.text = "\(marvelCharacter.availableComics! > 0 ? "" : "NO ")COMICS"
                        
                        MarvelApiClient.getMarvelComicsForCharacter(marvelCharacter, pageSize: 20, pageIndex: 0) { (marvelComics, totalAvailable) -> Void in
                            self.comicsCoverListCell!.datasource = marvelComics
                            self.comicsCoverListCell!.actionForItemSelect = { (indexPath) in
                                let fullScreenCoverListViewController = self.getFullScreenCoverListViewController()
                                fullScreenCoverListViewController.datasource = self.comicsCoverListCell!.datasource
                                fullScreenCoverListViewController.currentListItemIndex = indexPath.item
                                self.presentViewController(fullScreenCoverListViewController.navigationController!, animated: true, completion: nil)
                            }
                        }
                    }
                    return self.comicsCoverListCell!
                case 3:
                    if self.seriesCoverListCell == nil {
                        self.seriesCoverListCell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerCoverListTableViewCell") as? DetailViewControllerCoverListTableViewCell
                        self.seriesCoverListCell!.activityIndicator.startAnimating()
                        self.seriesCoverListCell!.titleTextLabel.text = "\(marvelCharacter.availableSeries! > 0 ? "" : "NO ")SERIES"
                        
                        MarvelApiClient.getMarvelSeriesForCharacter(marvelCharacter, pageSize: 20, pageIndex: 0) { (marvelSeries, totalAvailable) -> Void in
                            self.seriesCoverListCell!.datasource = marvelSeries
                            self.seriesCoverListCell!.actionForItemSelect = { (indexPath) in
                                let fullScreenCoverListViewController = self.getFullScreenCoverListViewController()
                                fullScreenCoverListViewController.datasource = self.seriesCoverListCell!.datasource
                                fullScreenCoverListViewController.currentListItemIndex = indexPath.item
                                self.presentViewController(fullScreenCoverListViewController.navigationController!, animated: true, completion: nil)
                            }
                        }
                    }
                    return self.seriesCoverListCell!
                case 4:
                    if self.storiesCoverListCell == nil {
                        self.storiesCoverListCell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerCoverListTableViewCell") as? DetailViewControllerCoverListTableViewCell
                        self.storiesCoverListCell!.activityIndicator.startAnimating()
                        self.storiesCoverListCell!.titleTextLabel.text = "\(marvelCharacter.availableStories! > 0 ? "" : "NO ")STORIES"
                        
                        MarvelApiClient.getMarvelStoriesForCharacter(marvelCharacter, pageSize: 20, pageIndex: 0) { (marvelStories, totalAvailable) -> Void in
                            self.storiesCoverListCell!.datasource = marvelStories
                            self.storiesCoverListCell!.actionForItemSelect = { (indexPath) in
                                let fullScreenCoverListViewController = self.getFullScreenCoverListViewController()
                                fullScreenCoverListViewController.datasource = self.storiesCoverListCell!.datasource
                                fullScreenCoverListViewController.currentListItemIndex = indexPath.item
                                self.presentViewController(fullScreenCoverListViewController.navigationController!, animated: true, completion: nil)
                            }
                        }
                    }
                    return self.storiesCoverListCell!
                case 5:
                    if self.eventsCoverListCell == nil {
                        self.eventsCoverListCell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerCoverListTableViewCell") as? DetailViewControllerCoverListTableViewCell
                        self.eventsCoverListCell!.activityIndicator.startAnimating()
                        self.eventsCoverListCell!.titleTextLabel.text = "\(marvelCharacter.availableEvents! > 0 ? "" : "NO ")EVENTS"
                        
                        MarvelApiClient.getMarvelEventsForCharacter(marvelCharacter, pageSize: 20, pageIndex: 0) { (marvelEvents, totalAvailable) -> Void in
                            self.eventsCoverListCell!.datasource = marvelEvents
                            self.eventsCoverListCell!.actionForItemSelect = { (indexPath) in
                                let fullScreenCoverListViewController = self.getFullScreenCoverListViewController()
                                fullScreenCoverListViewController.datasource = self.eventsCoverListCell!.datasource
                                fullScreenCoverListViewController.currentListItemIndex = indexPath.item
                                self.presentViewController(fullScreenCoverListViewController.navigationController!, animated: true, completion: nil)
                            }
                        }
                    }
                    return self.eventsCoverListCell!
                    
                default:
                    return UITableViewCell(frame: CGRectZero)
                }
            }
        }
        else if indexPath.section == 1 {
            if let marvelCharacter = self.detailItem as? MarvelCharacter {
                let cell = self.tableView.dequeueReusableCellWithIdentifier("DetailViewControllerLinkTableViewCell")!
                cell.textLabel!.text = marvelCharacter.relatedLinks[indexPath.row].linkType
                return cell;
            }
        }

        return UITableViewCell(frame: CGRectZero)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            if let marvelCharacter = self.detailItem as? MarvelCharacter {
                if let url = marvelCharacter.relatedLinks[indexPath.row].linkUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
        }
    }
    
    func getFullScreenCoverListViewController() -> FullScreenCoverListViewController {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let fullScreenCoverListViewNavigationController = mainStoryboard.instantiateViewControllerWithIdentifier("FullScreenCoverListViewNavigationController") as! UINavigationController
        
        return fullScreenCoverListViewNavigationController.topViewController as! FullScreenCoverListViewController
    }
}



