//
//  marvel_libraryTests.swift
//  marvel-libraryTests
//
//  Created by Spencer Diniz on 10/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import XCTest
@testable import marvel_library

class marvel_libraryTests: XCTestCase {
    
    func testMarvelCharacter() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelCharacter") as? Dictionary<String, AnyObject> {
            let marvelCharacter = MarvelCharacter(attributes: dictionary)
            XCTAssertEqual(marvelCharacter.marvelId, 1011334)
            XCTAssertEqual(marvelCharacter.marvelName, "3-D Man")
            XCTAssertEqual(marvelCharacter.marvelDescription, "This is 3-D Man. He needs no description.")
            XCTAssertEqual(marvelCharacter.marvelImageExtension, "jpg")
            XCTAssertEqual(marvelCharacter.marvelImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784")
            XCTAssertEqual(marvelCharacter.availableComics, 11)
            XCTAssertEqual(marvelCharacter.availableSeries, 2)
            XCTAssertEqual(marvelCharacter.availableStories, 17)
            XCTAssertEqual(marvelCharacter.availableEvents, 1)
            XCTAssertEqual(marvelCharacter.relatedLinks.count, 3)
            XCTAssertEqual(marvelCharacter.relatedLinks[0].linkType, "detail")
            XCTAssertEqual(marvelCharacter.relatedLinks[0].linkUrl?.URLString, "http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=a3c7dcea01fc2ede952dafd809e44e9c")
            XCTAssertEqual(marvelCharacter.relatedLinks[1].linkType, "wiki")
            XCTAssertEqual(marvelCharacter.relatedLinks[1].linkUrl?.URLString, "http://marvel.com/universe/3-D_Man_(Chandler)?utm_campaign=apiRef&utm_source=a3c7dcea01fc2ede952dafd809e44e9c")
            XCTAssertEqual(marvelCharacter.relatedLinks[2].linkType, "comiclink")
            XCTAssertEqual(marvelCharacter.relatedLinks[2].linkUrl?.URLString, "http://marvel.com/comics/characters/1011334/3-d_man?utm_campaign=apiRef&utm_source=a3c7dcea01fc2ede952dafd809e44e9c")
        }
        else {
            XCTFail("Unable to load MarvelCharacter.json file.")
        }
    }
    
    func testMarvelComic() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelComic") as? Dictionary<String, AnyObject> {
            let marvelComic = MarvelComic(attributes: dictionary)
            XCTAssertEqual(marvelComic.marvelId, 10225)
            XCTAssertEqual(marvelComic.marvelName, "Marvel Premiere (1972) #37")
            XCTAssertEqual(marvelComic.marvelDescription, "This is the comic description.")
            XCTAssertEqual(marvelComic.comicIssueNumber, 37)
            XCTAssertEqual(marvelComic.marvelImageExtension, "jpg")
            XCTAssertEqual(marvelComic.marvelImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available")
        }
        else {
            XCTFail("Unable to load MarvelComic.json file.")
        }
    }
    
    func testMarvelSeries() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelSeries") as? Dictionary<String, AnyObject> {
            let marvelSeries = MarvelSeries(attributes: dictionary)
            XCTAssertEqual(marvelSeries.marvelId, 1945)
            XCTAssertEqual(marvelSeries.marvelName, "Avengers: The Initiative (2007 - 2010)")
            XCTAssertEqual(marvelSeries.marvelDescription, "Series description.")
            XCTAssertEqual(marvelSeries.seriesStartYear, 2007)
            XCTAssertEqual(marvelSeries.seriesEndYear, 2010)
            XCTAssertEqual(marvelSeries.marvelImageExtension, "jpg")
            XCTAssertEqual(marvelSeries.marvelImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/514a2ed3302f5")
        }
        else {
            XCTFail("Unable to load MarvelSeries.json file.")
        }
    }
    
    func testMarvelStory() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelStory") as? Dictionary<String, AnyObject> {
            let marvelStory = MarvelStory(attributes: dictionary)
            XCTAssertEqual(marvelStory.marvelId, 49889)
            XCTAssertEqual(marvelStory.marvelName, "Avengers: The Initiative (2007) #19 - Int")
            XCTAssertEqual(marvelStory.marvelDescription, "Some story description.")
            XCTAssertEqual(marvelStory.storyType, "story")
            XCTAssertEqual(marvelStory.marvelImageExtension, "jpg")
            XCTAssertEqual(marvelStory.marvelImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/5/a0/514a2ed3302f5")
        }
        else {
            XCTFail("Unable to load MarvelStory.json file.")
        }
    }

    func testMarvelEvent() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelEvent") as? Dictionary<String, AnyObject> {
            let marvelEvent = MarvelEvent(attributes: dictionary)
            XCTAssertEqual(marvelEvent.marvelId, 269)
            XCTAssertEqual(marvelEvent.marvelName, "Secret Invasion")
            XCTAssertEqual(marvelEvent.marvelDescription, "The shape-shifting Skrulls have been infiltrating the Earth for years, replacing many of Marvel's heroes with impostors, setting the stage for an all-out invasion.")
            XCTAssertEqual(marvelEvent.eventStart, UnitTestUtil.dateFrom(2008, month: 6, day: 2))
            XCTAssertEqual(marvelEvent.eventEnd, UnitTestUtil.dateFrom(2009, month: 1, day: 25))
            XCTAssertEqual(marvelEvent.marvelImageExtension, "jpg")
            XCTAssertEqual(marvelEvent.marvelImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/6/70/51ca1749980ae")
        }
        else {
            XCTFail("Unable to load MarvelEvent.json file.")
        }
    }

    func testMarvelLink() {
        if let dictionary = UnitTestUtil.loadJsonFromFile("MarvelLink") as? Dictionary<String, AnyObject> {
            let marvelLink = MarvelLink(attributes: dictionary)
            XCTAssertEqual(marvelLink.linkType, "detail")
            XCTAssertEqual(marvelLink.linkUrl?.URLString, "http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=a3c7dcea01fc2ede952dafd809e44e9c")
        }
        else {
            XCTFail("Unable to load MarvelLink.json file.")
        }
    }
}
