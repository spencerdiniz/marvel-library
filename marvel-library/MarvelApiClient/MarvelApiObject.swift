//
//  MarvelApiObject.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MarvelApiObject: NSObject {
    var marvelId: Int?
    var marvelName: String?
    var marvelDescription: String?
    var marvelImagePath: String?
    var marvelImageExtension: String?
}
