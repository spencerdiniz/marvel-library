//
//  MarvelLink.swift
//  marvel-library
//
//  Created by Spencer Diniz on 14/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MarvelLink: NSObject {
    var linkType: String?
    var linkUrl: NSURL?
    
    convenience init(attributes: Dictionary<String, AnyObject>) {
        self.init()
        
        self.linkType = attributes["type"] as? String
        
        if let urlString = attributes["url"] as? String {
            self.linkUrl = NSURL(string: urlString)
        }
    }
}
