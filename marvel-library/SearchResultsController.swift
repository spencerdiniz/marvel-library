//
//  SearchResultsController.swift
//  marvel-library
//
//  Created by Spencer Diniz on 11/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit
import Alamofire

class SearchResultsController: UITableViewController, UISearchResultsUpdating {

    var datasource: Array<MarvelCharacter>! = []
    var request: Request?
    var itemSelectAction: ((selectedItem: MarvelCharacter) -> Void)?
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.request?.cancel()
        
        if let searchText = searchController.searchBar.text {
            if !searchText.isEmpty {
                self.request = MarvelApiClient.getMarvelCharacters(searchText, pageSize: 20, pageIndex: 0) { (marvelCharacters, totalAvailable) -> Void in
                    self.datasource = marvelCharacters
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let marvelCharacter = self.datasource[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("SearchResultsControllerTableViewCell", forIndexPath: indexPath) as? SearchResultsControllerTableViewCell
        
        cell?.loadMarvelApiObject(marvelCharacter)
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let marvelCharacter = self.datasource[indexPath.row]
        
        if self.itemSelectAction != nil {
            self.itemSelectAction!(selectedItem: marvelCharacter)
        }
    }
}
