//
//  UnitTestUtil.swift
//  marvel-library
//
//  Created by Spencer Diniz on 13/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class UnitTestUtil: NSObject {

    static func loadJsonFromFile(fileName: String) -> AnyObject? {
        
        if let path = NSBundle(forClass: UnitTestUtil.self).pathForResource(fileName, ofType: "json") {
            if let data = NSData(contentsOfFile: path) {
                do {
                    let json = try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                    return json
                }
            }
        }
        
        return nil
    }
    
    static func dateFrom(year: Int, month: Int, day: Int) -> NSDate? {
        let calendar = NSCalendar.currentCalendar()

        let components = NSDateComponents()
        components.year = year
        components.month = month
        components.day = day
        
        let date = calendar.dateFromComponents(components)
        
        return date
    }
}
