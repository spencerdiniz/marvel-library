//
//  MarvelApiClient.swift
//  marvel-library
//
//  Created by Spencer Diniz on 10/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

enum MarvelImageType {
    case PortraitThumbnail
    case SquareThumbnail
    case SquareThumbnailSmall
    case FullSize
}

class MarvelApiClient: NSObject {
    
    private static let baseApiUrl = NSURL(string: "http://gateway.marvel.com:80/v1/public/")
    private static let privateKey = "c1aeb1c0cb7831c8ac8966ab61b4fce2938a31f4"
    private static let publicKey = "a3c7dcea01fc2ede952dafd809e44e9c"
    
    private class func getAuthorizationParameters() -> (timestamp: String, hash: String) {
        let timestamp = String(NSDate().timeIntervalSince1970)
        let inputString = "\(timestamp)\(self.privateKey)\(self.publicKey)"
        
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = inputString.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var hash = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            hash += String(format: "%02x", digest[index])
        }
        
        return (timestamp, hash)
    }

    class func getMarvelCharacters(nameStartsWith: String?, pageSize: Int, pageIndex: Int, completion: (marvelCharacters: Array<MarvelCharacter>?, totalAvailable: Int) -> Void) -> Request {
        let autorization = self.getAuthorizationParameters()

        var requestParameters: Dictionary<String, AnyObject> = [
            "ts": autorization.timestamp,
            "hash": autorization.hash,
            "apikey": self.publicKey,
            "limit": pageSize,
            "offset": pageSize * pageIndex,
            "orderBy": "name"
        ]
        
        if nameStartsWith != nil {
            requestParameters["nameStartsWith"] = nameStartsWith
        }
        
        let marvelCharactersEndpoint = "\(self.baseApiUrl!.absoluteString)characters"
        
        let request = Alamofire.request(.GET, marvelCharactersEndpoint, parameters: requestParameters)
            .response(completionHandler: { request, response, data, error -> Void in
                if error != nil {
                    return
                }
            })
            .responseJSON { response in
                var marvelCharacters = Array<MarvelCharacter>()
                var totalAvailable = 0

                let json = response.result.value as? Dictionary<String, AnyObject>
                if let data = json?["data"] as? Dictionary<String, AnyObject> {
                    if let total = data["total"] as? Int {
                        totalAvailable = total
                        if let results = data["results"] as? Array<Dictionary<String, AnyObject>> {
                            for characterDictionary in results {
                                let marvelCharacter = MarvelCharacter(attributes: characterDictionary)
                                marvelCharacters.append(marvelCharacter)
                            }
                        }
                    }
                }
                
                completion(marvelCharacters: marvelCharacters, totalAvailable: totalAvailable)
        }
        
        return request
    }
    
    class func getMarvelComicsForCharacter(marvelCharacter: MarvelCharacter, pageSize: Int, pageIndex: Int, completion: (marvelComics: Array<MarvelComic>, totalAvailable: Int) -> Void) -> Request {
        let autorization = self.getAuthorizationParameters()
        
        let requestParameters: Dictionary<String, AnyObject> = [
            "ts": autorization.timestamp,
            "hash": autorization.hash,
            "apikey": self.publicKey,
            "limit": pageSize,
            "offset": pageSize * pageIndex,
            "orderBy": "-issueNumber"
        ]
        
        let marvelComicsEndpoint = "\(self.baseApiUrl!.absoluteString)characters/\(marvelCharacter.marvelId!)/comics"
        
        let request = Alamofire.request(.GET, marvelComicsEndpoint, parameters: requestParameters)
            .response(completionHandler: { request, response, data, error -> Void in
                if error != nil {
                    return
                }
            })
            .responseJSON { response in
                var marvelComics = Array<MarvelComic>()
                var totalAvailable = 0
                
                let json = response.result.value as? Dictionary<String, AnyObject>
                if let data = json?["data"] as? Dictionary<String, AnyObject> {
                    if let total = data["total"] as? Int {
                        totalAvailable = total
                        if let results = data["results"] as? Array<Dictionary<String, AnyObject>> {
                            for comicDictionary in results {
                                let marvelComic = MarvelComic(attributes: comicDictionary)
                                marvelComics.append(marvelComic)
                            }
                        }
                    }
                }
                
                completion(marvelComics: marvelComics, totalAvailable: totalAvailable)
        }
        
        return request
    }
    
    class func getMarvelSeriesForCharacter(marvelCharacter: MarvelCharacter, pageSize: Int, pageIndex: Int, completion: (marvelSeries: Array<MarvelSeries>, totalAvailable: Int) -> Void) -> Request {
        let autorization = self.getAuthorizationParameters()
        
        let requestParameters: Dictionary<String, AnyObject> = [
            "ts": autorization.timestamp,
            "hash": autorization.hash,
            "apikey": self.publicKey,
            "limit": pageSize,
            "offset": pageSize * pageIndex,
            "orderBy": "-startYear"
        ]
        
        let marvelSeriesEndpoint = "\(self.baseApiUrl!.absoluteString)characters/\(marvelCharacter.marvelId!)/series"
        
        let request = Alamofire.request(.GET, marvelSeriesEndpoint, parameters: requestParameters)
            .response(completionHandler: { request, response, data, error -> Void in
                if error != nil {
                    return
                }
            })
            .responseJSON { response in
                var marvelSeries = Array<MarvelSeries>()
                var totalAvailable = 0
                
                let json = response.result.value as? Dictionary<String, AnyObject>
                if let data = json?["data"] as? Dictionary<String, AnyObject> {
                    if let total = data["total"] as? Int {
                        totalAvailable = total
                        if let results = data["results"] as? Array<Dictionary<String, AnyObject>> {
                            for seriesDictionary in results {
                                let series = MarvelSeries(attributes: seriesDictionary)
                                marvelSeries.append(series)
                            }
                        }
                    }
                }
                
                completion(marvelSeries: marvelSeries, totalAvailable: totalAvailable)
        }
        
        return request
    }

    class func getMarvelStoriesForCharacter(marvelCharacter: MarvelCharacter, pageSize: Int, pageIndex: Int, completion: (marvelStories: Array<MarvelStory>, totalAvailable: Int) -> Void) -> Request {
        let autorization = self.getAuthorizationParameters()
        
        let requestParameters: Dictionary<String, AnyObject> = [
            "ts": autorization.timestamp,
            "hash": autorization.hash,
            "apikey": self.publicKey,
            "limit": pageSize,
            "offset": pageSize * pageIndex,
            "orderBy": "-modified"
        ]
        
        let marvelStoriesEndpoint = "\(self.baseApiUrl!.absoluteString)characters/\(marvelCharacter.marvelId!)/stories"
        
        let request = Alamofire.request(.GET, marvelStoriesEndpoint, parameters: requestParameters)
            .response(completionHandler: { request, response, data, error -> Void in
                if error != nil {
                    return
                }
            })
            .responseJSON { response in
                var marvelStories = Array<MarvelStory>()
                var totalAvailable = 0
                
                let json = response.result.value as? Dictionary<String, AnyObject>
                if let data = json?["data"] as? Dictionary<String, AnyObject> {
                    if let total = data["total"] as? Int {
                        totalAvailable = total
                        if let results = data["results"] as? Array<Dictionary<String, AnyObject>> {
                            for storyDictionary in results {
                                let marvelStory = MarvelStory(attributes: storyDictionary)
                                marvelStories.append(marvelStory)
                            }
                        }
                    }
                }
                
                completion(marvelStories: marvelStories, totalAvailable: totalAvailable)
        }
        
        return request
    }
    
    class func getMarvelEventsForCharacter(marvelCharacter: MarvelCharacter, pageSize: Int, pageIndex: Int, completion: (marvelEvents: Array<MarvelEvent>, totalAvailable: Int) -> Void) -> Request {
        let autorization = self.getAuthorizationParameters()
        
        let requestParameters: Dictionary<String, AnyObject> = [
            "ts": autorization.timestamp,
            "hash": autorization.hash,
            "apikey": self.publicKey,
            "limit": pageSize,
            "offset": pageSize * pageIndex,
            "orderBy": "-modified"
        ]
        
        let marvelEventsEndpoint = "\(self.baseApiUrl!.absoluteString)characters/\(marvelCharacter.marvelId!)/events"
        
        let request = Alamofire.request(.GET, marvelEventsEndpoint, parameters: requestParameters)
            .response(completionHandler: { request, response, data, error -> Void in
                if error != nil {
                    return
                }
            })
            .responseJSON { response in
                var marvelEvents = Array<MarvelEvent>()
                var totalAvailable = 0
                
                let json = response.result.value as? Dictionary<String, AnyObject>
                if let data = json?["data"] as? Dictionary<String, AnyObject> {
                    if let total = data["total"] as? Int {
                        totalAvailable = total
                        if let results = data["results"] as? Array<Dictionary<String, AnyObject>> {
                            for eventDictionary in results {
                                let marvelEvent = MarvelEvent(attributes: eventDictionary)
                                marvelEvents.append(marvelEvent)
                            }
                        }
                    }
                }
                
                completion(marvelEvents: marvelEvents, totalAvailable: totalAvailable)
        }
        
        return request
    }
    
    class func getMarvelImageForMarvelApiObject(marvelApiObject: MarvelApiObject, marvelImageType: MarvelImageType, completion: (image: UIImage?) -> Void) {
        
        var imageTypeString = ""
        
        switch marvelImageType {
        case .PortraitThumbnail:
            imageTypeString = "portrait_uncanny"
        case .SquareThumbnail:
            imageTypeString = "standard_fantastic"
        case .SquareThumbnailSmall:
            imageTypeString = "standard_large"
        case .FullSize:
            imageTypeString = "detail"
        }
        
        if let imagePath = marvelApiObject.marvelImagePath, imageExtension = marvelApiObject.marvelImageExtension  where !imagePath.containsString("image_not_available") {
            let imageUrl = "\(imagePath)/\(imageTypeString).\(imageExtension)"
            
            Alamofire
                .request(.GET, imageUrl)
                .responseImage { response in
                    completion(image: response.result.value)
            }
        }
        else {
            let imageName = "unavailable_\(imageTypeString)"
            let image = UIImage(named: imageName)
            
            completion(image: image)
        }
    }
}
