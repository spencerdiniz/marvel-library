//
//  MasterViewControllerTableViewCell.swift
//  marvel-library
//
//  Created by Spencer Diniz on 12/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MasterViewControllerTableViewCell: UITableViewCell {
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var characterImageImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = UIEdgeInsetsZero
    }

    override func prepareForReuse() {
        self.characterImageImageView.image = nil
        self.characterNameLabel.text = nil
        self.activityIndicator.stopAnimating()
    }

    func loadMarvelCharacter(marvelCharacter: MarvelCharacter) {
        self.activityIndicator.startAnimating()
        self.characterNameLabel.text = marvelCharacter.marvelName;
        MarvelApiClient.getMarvelImageForMarvelApiObject(marvelCharacter, marvelImageType: .SquareThumbnail) { (image) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.characterImageImageView.image = image
                self.activityIndicator.stopAnimating()
            })
        }
    }
}
