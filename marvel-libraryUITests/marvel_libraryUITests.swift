//
//  marvel_libraryUITests.swift
//  marvel-libraryUITests
//
//  Created by Spencer Diniz on 10/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import XCTest

class marvel_libraryUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    func testSearch() {
        let app = XCUIApplication()
        let masterNavigationBar = app.navigationBars["Master"]
        
        let searchButton = masterNavigationBar.buttons["icn nav search"]
        XCTAssertTrue(searchButton.exists)
        masterNavigationBar.buttons["icn nav search"].tap()
        
        masterNavigationBar.searchFields["Search"].typeText("psylock")
        
        let cell = app.cells.staticTexts["Psylocke"]
        let exists = NSPredicate(format: "exists == 1")
        expectationForPredicate(exists, evaluatedWithObject: cell, handler: nil)
        waitForExpectationsWithTimeout(5, handler: nil)
        cell.tap()
        
        let nameLabel = app.tables.cells.staticTexts["Psylocke"]
        XCTAssertTrue(nameLabel.exists)
    }
    
}
