//
//  MasterViewController.swift
//  marvel-library
//
//  Created by Spencer Diniz on 10/03/16.
//  Copyright © 2016 Spencer. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UISearchBarDelegate, UISearchControllerDelegate {

    var datasource: Array<MarvelCharacter>! = []
    var searchController: UISearchController?
    var detailViewController: DetailViewController?
    var searchedMarvelCharacter: MarvelCharacter?
    var searchButton: UIBarButtonItem?
    var totalAvailable: Int = 0
    var currentPageIndex: Int = 0
    var fetchingData: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 160.0

        self.searchButton = self.navigationItem.rightBarButtonItem;
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            let navController = (controllers[controllers.count-1] as! UINavigationController)
            self.detailViewController = navController.topViewController as? DetailViewController
            self.navigationItem.titleView = UIImageView(image: UIImage(named: "icn-nav-marvel"))
        }
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let searchResultsController = mainStoryboard.instantiateViewControllerWithIdentifier("SearchResultsController") as! SearchResultsController
        self.definesPresentationContext = true
        self.searchController = UISearchController(searchResultsController: searchResultsController)
        self.searchController?.delegate = self
        self.searchController?.searchBar.delegate = self
        self.searchController?.searchResultsUpdater = searchResultsController
        self.searchController?.hidesNavigationBarDuringPresentation = true
        self.searchController?.hidesNavigationBarDuringPresentation = false
        
        searchResultsController.itemSelectAction = { (marvelCharacter) -> Void in
            self.searchedMarvelCharacter = marvelCharacter
            self.performSegueWithIdentifier("showDetail", sender: self)
        }
        
        self.fetchDataPage(0)
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.shadowImage = nil
    }

    @IBAction func searchButtonTapped(sender: AnyObject) {
        self.navigationItem.titleView = self.searchController?.searchBar
        self.searchController?.active = true
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func fetchDataPage(pageIndex: Int) {
        if !self.fetchingData {
            self.fetchingData = true
            MarvelApiClient.getMarvelCharacters(nil, pageSize: 20, pageIndex: pageIndex) { (marvelCharacters, totalAvailable) -> Void in
                self.totalAvailable = totalAvailable
                self.datasource.appendContentsOf(marvelCharacters!)
                self.tableView.reloadData()
                self.fetchingData = false
            }
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.navigationItem.titleView?.alpha = 0.0
            }) { (finished) -> Void in
                self.navigationItem.titleView = UIImageView(image: UIImage(named: "icn-nav-marvel"))
                self.navigationItem.rightBarButtonItem = self.searchButton
        }
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            self.searchController?.searchBar.becomeFirstResponder()
        }
    }
    
    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            var marvelCharacter: MarvelCharacter?
            
            if self.searchedMarvelCharacter != nil {
                marvelCharacter = self.searchedMarvelCharacter
                self.searchedMarvelCharacter = nil
            }
            else {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    marvelCharacter = self.datasource[indexPath.row]
                }
            }
            
            if marvelCharacter != nil {
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = marvelCharacter
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
                self.searchedMarvelCharacter = nil
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.datasource.count + (self.datasource.count < self.totalAvailable ? 1 : 0)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row > self.datasource.count - 1 {
            self.fetchDataPage(++self.currentPageIndex)
            let cell = tableView.dequeueReusableCellWithIdentifier("MasterViewControllerLoadingIndicatorTableViewCell", forIndexPath: indexPath) as! MasterViewControllerLoadingIndicatorTableViewCell
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("MasterViewControllerTableViewCell", forIndexPath: indexPath) as! MasterViewControllerTableViewCell
            let marvelCharacter = self.datasource[indexPath.row]
            cell.loadMarvelCharacter(marvelCharacter)
            
            return cell
        }
    }
}

